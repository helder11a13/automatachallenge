#include "MAX7317.h"
#include "automata_spi.h"
#include <stdio.h>

/*
	Configure the SPI driver to communicate with
	the MAX7317.
*/
MAX7317_RETURN_T MAX7317_ConfigSpi(void)
{
	SPI_CONFIG_T max7317SpiConfig;
	max7317SpiConfig.cs_pol			= SPI_ACTIVE_LOW;
	max7317SpiConfig.cpol 			= SPI_IDLE_LOW;
	max7317SpiConfig.cpha			= SPI_EDGE_RISING;
	max7317SpiConfig.bits_per_frame	= 16;
	max7317SpiConfig.baudrate		= 1625000;
	max7317SpiConfig.cs_to_sck		= 10;
	max7317SpiConfig.sck_to_cs		= 3;

	/* If the config. function returns != 0, something was wrong. */
	if(SPI_configureTransfer(&max7317SpiConfig))
		return MAX7317_RETURN_spi_config_err;

	return MAX7317_RETURN_ok;
}

/*
	Config individual ports
*/
MAX7317_RETURN_T MAX7317_ConfigPort(MAX7317_PORT_T *portConfig)
{
	uint8_t tx_data[2];
	uint8_t garbage[2];
	SPI_TRANSFER_T transfer;

	/*
		BUILD MESSAGE
		The value of the portNumber is the same as the register command. So 
		use the portNumber as the cmnd code in the message.
	*/
	tx_data[1] = portConfig->portNumber;	// Cmnd
	tx_data[0] = portConfig->direction;		// Data

	transfer.number_of_bytes = 2;
	transfer.tx_data = tx_data;
	transfer.rx_data = garbage;

	// If transferBlocking returns != 0, something was wrong.
	if(SPI_transferBlocking(&transfer))
		return MAX7317_RETURN_port_config_err;

	return MAX7317_RETURN_ok;
}

/* Read port level of the port indicated by portConfig*/
MAX7317_PORT_LEVEL_T MAX7317_ReadPort(MAX7317_PORT_T *portConfig)
{
	uint32_t inputReg = 0;	// Storage the input registers
	uint8_t tx_data[2];
	uint8_t rx_data[2];
	SPI_TRANSFER_T transfer;

	// When reading, only the command has to be send.
	// Two bytes of memmory have been allocated (tx_data[2]) for the sake of safety.
	tx_data[1] = MAX7317_READ_PORTS_0_7_CMND;	// Command to read ports level from 0-7

	transfer.number_of_bytes = 2;
	transfer.tx_data = tx_data;
	transfer.rx_data = rx_data;

	// The register needs to be read twice. The first read is rubbish from a 
	// previous transaction.
	if(SPI_transferBlocking(&transfer))
		return MAX7317_RETURN_port_read_err;
	tx_data[1] = MAX7317_READ_PORTS_8_9_CMND;	// Command to read ports level from 8-9
	if(SPI_transferBlocking(&transfer))
		return MAX7317_RETURN_port_read_err;
	// Save first useful data (ports 0-7)
	// The second byte is the only with useful data
	inputReg = rx_data[1];
	if(SPI_transferBlocking(&transfer))
		return MAX7317_RETURN_port_read_err;
	// Reads remaining data (8-9)
	// The two first bits of the second byte is the only useful data
	inputReg |= ((rx_data[1] & 0x03) << 8);

	// Return the level of the desired port.
	return (inputReg & (1 << portConfig->portNumber) ? MAX7317_PORT_HIGH : MAX7317_PORT_LOW);
}

/* Write port */
MAX7317_RETURN_T MAX7317_WritePort(MAX7317_PORT_T *portConfig, MAX7317_PORT_LEVEL_T portLevel)
{
	uint8_t tx_data[2];
	uint8_t garbage[2];
	SPI_TRANSFER_T transfer;

	// If the port is in input mode, this operation doesn't apply
	if(portConfig->direction == MAX7317_PORT_DIR_IN)
		return MAX7317_RETURN_port_write_err;

	tx_data[1] = portConfig->portNumber;
	tx_data[0] = portLevel;

	transfer.number_of_bytes = 2;
	transfer.tx_data = tx_data;
	transfer.rx_data = garbage;

	/* If the config. function returns != 0, something was wrong. */
	if(SPI_transferBlocking(&transfer))
		return MAX7317_RETURN_port_write_err;

	return MAX7317_RETURN_ok;
}
