TARGET = automata_test.out
SPI_LIB = lib/automata_spi.o
SPI_HEADER = lib/automata_spi.h
CC = gcc
CFLAGS = -g -Wall -Ilib

.PHONY: default all clean

default: $(TARGET)
all: default

USER_OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
USER_HEADERS = $(wildcard *.h)

OBJECTS = $(USER_OBJECTS) $(SPI_LIB)
HEADERS = $(USER_HEADERS) $(SPI_HEADER)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC)  $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)