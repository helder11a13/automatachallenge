#ifndef MAX7317_H_
#define MAX7317_H_

#define	READ	(0x08)
#define MAX7317_READ_PORTS_0_7_CMND	(READ | 0x0E)
#define MAX7317_READ_PORTS_8_9_CMND	(READ | 0x0F)

typedef enum {
	MAX7317_RETURN_ok,
	MAX7317_RETURN_spi_config_err,
	MAX7317_RETURN_port_config_err,
	MAX7317_RETURN_port_read_err,
	MAX7317_RETURN_port_write_err
} MAX7317_RETURN_T;

typedef enum {
	MAX7317_PORT_DIR_OUT,
	MAX7317_PORT_DIR_IN
} MAX7317_PORT_DIR_T;

typedef enum {
	PORT_0, PORT_1, PORT_2, PORT_3,
	PORT_4, PORT_5, PORT_6, PORT_7,
	PORT_8, PORT_9
} MAX7317_PORT_NUMB_T;

typedef struct MAX7317_PORT
{
	MAX7317_PORT_DIR_T	direction;
	MAX7317_PORT_NUMB_T	portNumber;
} MAX7317_PORT_T;

typedef enum {
	MAX7317_PORT_LOW, MAX7317_PORT_HIGH
} MAX7317_PORT_LEVEL_T;

MAX7317_RETURN_T 		MAX7317_ConfigSpi(void);
MAX7317_RETURN_T 		MAX7317_ConfigPort(MAX7317_PORT_T *portConfig);
MAX7317_PORT_LEVEL_T 	MAX7317_ReadPort(MAX7317_PORT_T *portConfig);
MAX7317_RETURN_T 		MAX7317_WritePort(MAX7317_PORT_T *portConfig, MAX7317_PORT_LEVEL_T portLevel);

#endif
