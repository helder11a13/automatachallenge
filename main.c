#include "MAX7317.h"
#include <stdio.h>

int main(void)
{
	MAX7317_PORT_T max7317Port0;
	MAX7317_PORT_T max7317Port1;
	int test;
	
	// Config. SPI to communicate to MAX7317
	if(MAX7317_ConfigSpi())
	{
		printf("Something was wrong with the MAX7317_SpiConfig.\n");
		return 1;
	}

	// Config por0 and port1
	max7317Port0.direction 	= MAX7317_PORT_DIR_IN;
	max7317Port0.portNumber = PORT_0;
	max7317Port1.direction 	= MAX7317_PORT_DIR_OUT;
	max7317Port1.portNumber = PORT_1;

	MAX7317_ConfigPort(&max7317Port0);
	MAX7317_ConfigPort(&max7317Port1);

	// Read port0
	test = MAX7317_ReadPort(&max7317Port0);
	printf("port0 level %d\n", test);

	// Write port1
	MAX7317_WritePort(&max7317Port1, MAX7317_PORT_LOW);

	return 0;
}
